import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import java.util.Optional;

public class WordCount {

    public static void main(String[] args) {

        String fileName = Optional.ofNullable(System.getenv("FILE_NAME")).orElse("data.txt");
        String wordToSearch = Optional.ofNullable(System.getenv("WORD_TO_SEARCH")).orElse("world");

        System.out.println("FILE_NAME: " + fileName);
        System.out.println("WORD_TO_SEARCH: " + wordToSearch);

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line;
            int wordCount = 0;

            while ((line = reader.readLine()) != null) {
                String[] words = line.split("\\s+");
                for (String word : words) {
                    if (word.equals(wordToSearch)) {
                        wordCount++;
                    }
                }
            }

            System.out.println("Occurrences of '" + wordToSearch + "': " + wordCount);
        } catch (IOException e) {
            System.err.println("An error occurred while reading the file: " + e.getMessage());
        }
    }
}
