FROM ubuntu:22.04 as java

RUN apt-get -qq update
RUN apt-get -qq install wget -y

RUN wget -q https://download.oracle.com/java/17/archive/jdk-17.0.8_linux-x64_bin.tar.gz
RUN tar -xf jdk-17.0.8_linux-x64_bin.tar.gz

FROM ubuntu:22.04 as mvn

RUN apt-get -qq update
RUN apt-get -qq install wget -y

RUN wget -q https://dlcdn.apache.org/maven/maven-3/3.9.4/binaries/apache-maven-3.9.4-bin.tar.gz
RUN tar -xf apache-maven-3.9.4-bin.tar.gz


FROM ubuntu:22.04 as main

ENV JAVA_HOME=/usr/local/jdk-17.0.8
ENV M2_HOME=/opt/apache-maven-3.9.4

ENV PATH=$PATH:$JAVA_HOME/bin:$M2_HOME/bin

COPY --from=java /jdk-17.0.8 /usr/local/jdk-17.0.8
COPY --from=mvn /apache-maven-3.9.4 /opt/apache-maven-3.9.4

RUN java --version
RUN mvn -version
