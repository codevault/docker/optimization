n = 200
input_file_path = 'sample.txt'
output_file_path = '../wordcount/data.txt'

try:
    with open(input_file_path, 'r') as input_file:
        input_content = input_file.read()

    with open(output_file_path, 'w') as output_file:
        for _ in range(n):
            output_file.write(input_content)
except FileNotFoundError:
    print(f"The input file '{input_file_path}' was not found.")
except Exception as e:
    print(f"An error occurred: {e}")
