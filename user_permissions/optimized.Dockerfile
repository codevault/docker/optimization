FROM openjdk:17 as build

RUN mkdir build
WORKDIR /build

COPY wordcount/WordCount.java .
RUN javac WordCount.java

FROM openjdk:17 as main

RUN useradd -ms /bin/bash java
WORKDIR /home/java

COPY --from=build --chown=java:java --chmod=770 /build/WordCount.class .
COPY --chown=java:java --chmod=770 wordcount/data.txt .

USER java

CMD ["java", "WordCount"]
