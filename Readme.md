# Docker Image Optimization with Dive
Docker images are a collection of change's done on the file system, these changes are stacked on top of each other.
Each change is called a layer. A layer can be viewed as a snapshot of the image after the change has been done.
Normally changes that happen in image layers are addition, modification or deletion of files/directories.
Modification could be the change in the content of the file or the user/permission change of the file/directory.
Dive tool allows us to see the changes that has happened as part of each layer.
We can use this information to optimize the size of our images.</p>
* Dive CodeBase: https://github.com/wagoodman/dive

## Dive Usage

### Configure Dive for Docker:
* `alias dive="docker run -ti --rm  -v /var/run/docker.sock:/var/run/docker.sock wagoodman/dive"`

### To Scan Image:
* `dive <image>:<tag>`

### To Build and Scan Image:
* `dive build -t <image>:<tag> -f <filename> <filepath>`

## [Docker Build Best Practices](https://docs.docker.com/build/cache/)
* Use minimal base images<br><br>
* Minimizing the number of layers - Consolidate RUN Commands
  * Merge Run Lazy<br>
    ![lazy](files/merge_run_lazy.jpg)
  * Merge Run Optimized<br>
    ![optimized](files/merge_run_optimized.jpg)
  * Unzipping and Moving Files: 
    * Expectation: Unzipping and moving files should not increase size of the image.
    * Actual: Unzipping adds a layer and moving adds another layer with same files moved to a separate location.
  * Deleting Files:
    * Expectation: Deleting files should free up space in the image.
    * Actual: Deletion adds a new layer where the files are removed, but the layer where the files were added is still part of the image.
  * Conclusion: If unzip, move and cleanup is done as part of a single RUN command, it is treated as a single layer and only the final state is added as part of the image layer.<br><br>
* Use Multistage builds
  * Multistage Build Lazy<br>
    ![lazy](files/multi_stage_build_lazy.jpg)
  * Multistage Build Optimized<br>
    ![optimized](files/multi_stage_build_optimized.jpg)
  * Conclusion: Operations that are not required to be in the main image can be run in a base image and can be copied to the main image. This avoids the need to optimize and cleanup the base image, as the base image will not be part of the main image,<br><br>
* Avoid chown and chmod
  * User Permissions Lazy<br>
    ![lazy](files/multi_stage_build_lazy.jpg)
  * User Permissions Optimized<br>
    ![optimized](files/multi_stage_build_optimized.jpg)
  * User and Permission Change:
    * Expectation: Changing user or changing permissions should not increase size of the image.
    * Actual: User change and permission change ass new lays with the new user and permission information.
  * Conclusion: User/Permission change is a very problematic operation. Use BuildKit features to ass user and permission when files are copied into the image either form base image or from local file system.<br><br>
* Optimize Package Install (1.Install only mandatory dependencies 2.Disable cache 3.Cleanup)
  * For Debian/Ubuntu:
    * apt-get install -y --no-install-recommends <list of package names to install> && <optional: do something with packages> && apt-get clean && rm -rf /var/lib/apt/lists/*
    * This ensures that no recommended packages are installed, and that the cache is cleared at the end.
  * For RPM-based systems, like RHEL:
    * dnf -y install --setopt=install_weak_deps=False <list of package names to install> && <optional: do something with packages> && dnf clean all
    * The “dnf clean all” command ensures that all caches are deleted.<br><br>
* Use [BuildKit](https://docs.docker.com/build/buildkit/#getting-started)<br><br>
* Use [.dockerignore](https://docs.docker.com/engine/reference/builder/#dockerignore-file)


## Dive Tool KeyBindings

| Key Binding                         | Description                                        |
|-------------------------------------|----------------------------------------------------|
| <kbd>Ctrl + C</kbd> or <kbd>Q</kbd> | Exit                                               |
| <kbd>Tab</kbd>                      | Switch between the layer and filetree views        |
| <kbd>Ctrl + F</kbd>                 | Filter files                                       |
| <kbd>PageUp</kbd>                   | Scroll up a page                                   |
| <kbd>PageDown</kbd>                 | Scroll down a page                                 |
| <kbd>Ctrl + A</kbd>                 | Layer view: see aggregated image modifications     |
| <kbd>Ctrl + L</kbd>                 | Layer view: see current layer modifications        |
| <kbd>Space</kbd>                    | Filetree view: collapse/uncollapse a directory     |
| <kbd>Ctrl + Space</kbd>             | Filetree view: collapse/uncollapse all directories |
| <kbd>Ctrl + A</kbd>                 | Filetree view: show/hide added files               |
| <kbd>Ctrl + R</kbd>                 | Filetree view: show/hide removed files             |
| <kbd>Ctrl + M</kbd>                 | Filetree view: show/hide modified files            |
| <kbd>Ctrl + U</kbd>                 | Filetree view: show/hide unmodified files          |
| <kbd>Ctrl + B</kbd>                 | Filetree view: show/hide file attributes           |
| <kbd>PageUp</kbd>                   | Filetree view: scroll up a page                    |
| <kbd>PageDown</kbd>                 | Filetree view: scroll down a page                  |