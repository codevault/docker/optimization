FROM ubuntu:22.04 as build

RUN apt-get -qq update
RUN apt-get -qq install wget -y

RUN wget -q https://download.oracle.com/java/17/archive/jdk-17.0.8_linux-x64_bin.tar.gz
RUN tar -xf jdk-17.0.8_linux-x64_bin.tar.gz
RUN mv jdk-17.0.8 /usr/local/
RUN rm -f jdk-17.0.8_linux-x64_bin.tar.gz

RUN wget -q https://dlcdn.apache.org/maven/maven-3/3.9.4/binaries/apache-maven-3.9.4-bin.tar.gz
RUN tar -xf apache-maven-3.9.4-bin.tar.gz
RUN mv apache-maven-3.9.4 /opt/
RUN rm -f apache-maven-3.9.4-bin.tar.gz

RUN update-alternatives --install /usr/bin/java java /usr/local/jdk-17.0.8/bin/java 1
RUN update-alternatives --install /usr/bin/javac javac /usr/local/jdk-17.0.8/bin/javac 1
RUN update-alternatives --config java
RUN update-alternatives --config javac

RUN ln -s /opt/apache-maven-3.9.4/bin/mvn /usr/local/bin/mvn

RUN java --version
RUN mvn -version
