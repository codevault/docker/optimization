FROM openjdk:17

RUN useradd -ms /bin/bash java
WORKDIR /home/java

COPY wordcount/WordCount.java .
RUN chown java:java WordCount.java
RUN chmod 770 WordCount.java

COPY wordcount/data.txt .
RUN chown java:java data.txt
RUN chmod 770 data.txt

USER java

RUN javac WordCount.java
RUN rm -f WordCount.java

CMD ["java", "WordCount"]
